package in.co.rshop.ems.entities;


import java.sql.Time;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Exams {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long ExamCode;
	private String ExamType;
	private int RoomNo;
	private String Date;
	private Time OpeningTime;


	public Time getOpeningTime() {
		return OpeningTime;
	}

	public void setOpeningTime(Time openingTime) {
		OpeningTime = openingTime;
	}

	public Time getClosingTime() {
		return ClosingTime;
	}

	public void setClosingTime(Time closingTime) {
		ClosingTime = closingTime;
	}

	private Time ClosingTime;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public long getExamCode() {
		return ExamCode;
	}

	public void setExamCode(long examCode) {
		ExamCode = examCode;
	}

	public String getExamType() {
		return ExamType;
	}

	public void setExamType(String examType) {
		ExamType = examType;
	}

	public int getRoomNo() {
		return RoomNo;
	}

	public void setRoomNo(int roomNo) {
		RoomNo = roomNo;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

}
