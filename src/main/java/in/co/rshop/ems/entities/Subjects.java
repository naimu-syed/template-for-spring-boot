package in.co.rshop.ems.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Subjects {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long SubId;
	private String SubjectName;
	private long ExamCode;

	public long getSubId() {
		return SubId;
	}

	public void setSubId(long SubId) {
		this.SubId = SubId;
	}

	public String getSubjectName() {
		return SubjectName;
	}

	public void setSubjectName(String SubjectName) {
		this.SubjectName = SubjectName;
	}

	@JoinColumn(name = "Exam_code")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public long getExamCode() {
		return ExamCode;
	}

	public void setExamCode(long examCode) {
		ExamCode = examCode;
	}

}
